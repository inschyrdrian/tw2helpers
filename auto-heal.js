(function () {
    var modelDataService = injector.get('modelDataService')
    var socketService = injector.get('socketService')
    var routeProvider = injector.get('routeProvider')

    var player = modelDataService.getSelectedCharacter()
    var villages = player.getVillageList()

    villages.forEach(function (village) {
        var hospital = village.hospital
        var patients = hospital.patients
        var healed = patients.healed
        var pending = patients.pending

        healed.forEach(function (heal) {
            
            // before release the units
            // check if the farm has space;

            socketService.emit(routeProvider.HOSPITAL_RELEASE_PATIENT, {
                village_id: village.getId(),
                patient_id: heal.id
            })
        })
    })
})()
