var rootScope = angular.element(document).scope()
var modelDataService = injector.get('modelDataService')
var socketService = injector.get('socketService')
var routeProvider = injector.get('routeProvider')
var eventTypeProvider = injector.get('eventTypeProvider')
var windowDisplayService = injector.get('windowDisplayService')
var windowManagerService = injector.get('windowManagerService')
var angularHotkeys = injector.get('hotkeys')
var armyService = injector.get('armyService')
var villageService = injector.get('villageService')
var $filter = injector.get('$filter')
var $player = modelDataService.getSelectedCharacter()
var GAME_STATES = require('conf/gameStates')
var $gameState = modelDataService.getGameState()
var $mapData = require('struct/MapData')
var selectedVillage = $player.getSelectedVillage()
var gameData = modelDataService.getGameData()
var groupService = injector.get('groupService')
var overviewService = injector.get('overviewService')

require([
	'conf/unitTypes',
	'conf/unitCategories',
	'conf/buildingTypes'
], function (
	unitTypes,
	unitCategories,
	buildingTypes
) {
	var getUnitCategory = function getUnitCategory(unit) {
		switch (unit) {
			case unitTypes.SPEAR:
			case unitTypes.SWORD:
			case unitTypes.AXE:
			case unitTypes.ARCHER:
			case unitTypes.DOPPELSOLDNER:
				return unitCategories.INFANTRY;
			case unitTypes.LIGHT_CAVALRY:
			case unitTypes.HEAVY_CAVALRY:
			case unitTypes.MOUNTED_ARCHER:
			case unitTypes.KNIGHT:
				return unitCategories.CAVALRY;
			case unitTypes.RAM:
			case unitTypes.CATAPULT:
			case unitTypes.TREBUCHET:
				return unitCategories.SIEGE;
			case unitTypes.SNOB:
				return unitCategories.SPECIAL;
			default:
				return null;
		}
	}

	var calculateDiscipline = function calculateDiscipline(army) {
		var baseDiscipline,
			upgradeBonus,
			discipline;

		if (!army.officers) {
			return 0;
		}

		baseDiscipline = getBaseDiscipline(army.units);
		upgradeBonus = getUpgradeBonus();
		discipline = Math.min(baseDiscipline + upgradeBonus, 10);

		console.log('baseDiscipline', baseDiscipline)
		console.log('upgradeBonus', upgradeBonus)
		console.log('discipline', discipline)
		console.log('result', (10 - discipline) / 2)

		return (10 - discipline) / 2;
	}

	var getBaseDiscipline = function getBaseDiscipline(units) {
		var unit,
			category,
			categories = {};

		for (unit in units) {
			if (units[unit] > 0) {
				category = getUnitCategory(unit);

				if (!categories[category]) {
					categories[category] = true;
				}
			}
		}

		return 4 - Object.keys(categories).length;
	}

	var getUpgradeBonus = function getUpgradeBonus() {
		var bonusUpgrades = ['training_ground', 'large_ground', 'military_academy'],
			i = 0,
			bonus = 0,
			researches = modelDataService.getSelectedVillage().getBuildingData().getDataForBuilding(buildingTypes.BARRACKS).researches;
			
		for (i = 0; i < bonusUpgrades.length; i++) {
			if (researches[bonusUpgrades[i]]) {
				if (researches[bonusUpgrades[i]].researched || researches[bonusUpgrades[i]].unlocked) {
					bonus += 3;
				}
			}
		}

		return bonus;
	}

	var c = calculateDiscipline({
		units: {
			spear: 1000,
			sword: 100,
			axe: 1000,
			archer: 100,
			light_cavalry: 10,
			mounted_archer: 10,
			snob: 10,
			ram: 10,
			heavy_cavalry: 1000
		},
		officers: {}
	})

	console.log('calculateDiscipline()', c)
})
