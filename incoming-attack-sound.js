{
	let rootScope = angular.element(document).scope(),
		modelDataService = injector.get('modelDataService'),
		eventTypeProvider = injector.get('eventTypeProvider'),
		$player = modelDataService.getPlayer(),
		siren = new Audio('https://my.mixtape.moe/pgvtdz.mp3')

	rootScope.$on(eventTypeProvider.COMMAND_INCOMING, function (event, data) {
		if (data.type === 'attack' && data.direction === 'forward') {
			siren.play()
		}
	})
}
