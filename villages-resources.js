(function () {
    var modelDataService = injector.get('modelDataService')
    var villageService = injector.get('villageService')

    var player = modelDataService.getSelectedCharacter()
    var villages = player.getVillageList()

    villages.forEach(function (village) {
        // always check if the village is initialized and ready.
        // accounts with many villages only initialize some by default.
        
        villageService.ensureVillageDataLoaded(village.getId(), function () {
            // not sure if it's needed.
            if (!village.isInitialized()) {
                villageService.initializeVillage(village)
            }

            var resources = village.getResources()
            var computed = resources.getComputed()
            var maxStorage = resources.getMaxStorage()
            
            console.log('resources', computed)
            console.log('storage', maxStorage)
        })
    })
})()
