{
    "command_id": 7250611,
    "command_type": "attack",
    "time_start": 1527905560,
    "time_completed": 1527945460,
    "command_name": "",
    "target_village_id": 9073,
    "target_village_name": "Acerola Orion",
    "target_x": 484,
    "target_y": 586,
    "target_province_id": 3088,
    "target_province_name": "Balheimkan",
    "target_continent_id": 29,
    "target_continent_name": "Hohnfol",
    "target_groups": [{
        "id": 11916,
        "name": "Ignore Farm",
        "icon": 787
    }, {
        "id": 11914,
        "name": "Church",
        "icon": 2581
    }, {
        "id": 11911,
        "name": "Offensive",
        "icon": 257
    }],
    "origin_village_id": 6635,
    "origin_village_name": "NO MERCY",
    "origin_x": 474,
    "origin_y": 567,
    "origin_province_id": 3006,
    "origin_province_name": "Folfing",
    "origin_continent_id": 29,
    "origin_continent_name": "Hohnfol",
    "origin_groups": [],
    "origin_character_id": 1094601,
    "origin_character_name": "Don Violent",
    "targetVillage": {
        "name": "Acerola Orion",
        "x": 484,
        "y": 586,
        "id": 9073,
        "own": true
    },
    "targetCharacter": {},
    "originVillage": {
        "name": "NO MERCY",
        "x": 474,
        "y": 567,
        "id": 6635,
        "own": false
    },
    "originCharacter": {
        "name": "Don Violent",
        "id": 1094601
    },
    "model": {
        "data": {
            "id": 7250611,
            "start_village_id": 6635,
            "start_village_name": "NO MERCY",
            "time_completed": 1527945460,
            "time_start": 1527905560,
            "type": "attack",
            "x": 474,
            "y": 567,
            "home": {
                "id": 6635,
                "name": "NO MERCY"
            },
            "target": {}
        },
        "time_start": 1527905560,
        "time_completed": 1527945460,
        "jobType": "command",
        "id": 7250611,
        "type": "attack",
        "startVillageId": 6635,
        "startVillageName": "NO MERCY",
        "startCharacterName": null,
        "startCharacterId": null,
        "targetCharacterName": null,
        "targetCharacterId": null,
        "targetIsBarbarian": false,
        "returning": false,
        "units": null,
        "isSpy": false,
        "isCommand": true,
        "actionType": "attack",
        "destination": {
            "village": {},
            "character": {
                "name": null,
                "id": null
            }
        }
    }
}
