var $root = angular.element(document).scope()
var $model = injector.get('modelDataService')
var $socket = injector.get('socketService')
var $route = injector.get('routeProvider')
var $eventType = injector.get('eventTypeProvider')
var $filter = injector.get('$filter')
var $wds = injector.get('windowDisplayService')
var $wms = injector.get('windowManagerService')
var $hotkeys = injector.get('hotkeys')
var $armyService = injector.get('armyService')
var $villageService = injector.get('villageService')
var $presetList = $model.getPresetList()
var readableDateFilter = $filter('readableDateFilter')
var readableMillisecondsFilter = $filter('readableMillisecondsFilter')
var $player = $model.getSelectedCharacter()
var $selectedVillage = $player.getSelectedVillage()
var $timeHelper = require('helper/time')

var parseIcon = function (icon) {
    return typeof icon === 'string' ? parseInt(icon, 16) : icon
}
 
var tools = {}
 
tools.createPreset = function (name, units, icon) {
    var presetAlreadyCreated = utils.getPresetByName(name)
 
    var presetData = {
        village_id: $selectedVillage.getId(),
        name: name,
        icon: typeof icon === 'string' ? parseInt(icon, 16) : icon,
        officers: {},
        units: units,
        catapult_target: null
    }
 
    if (presetAlreadyCreated) {
        presetData.preset_id = presetAlreadyCreated.id
        $socket.emit($route.SAVE_EXISTING_PRESET, presetData)
    } else {
        $socket.emit($route.SAVE_NEW_PRESET, presetData)
    }
}
 
tools.createGroup = function (name, icon) {
    var groupAlreadyCreated = utils.getGroupByName(name)
 
    var data = {
        name: name,
        icon: parseIcon(icon)
    }
 
    if (groupAlreadyCreated) {
        data.id = groupAlreadyCreated.id
        $socket.emit($route.GROUPS_UPDATE, data)
    } else {
        $socket.emit($route.GROUPS_CREATE, data)
    }
}
 
tools.deleteAllGroups = function () {
    var groups = $model.getGroupList().getGroups()
 
    for (var id in groups) {
        $socket.emit($route.GROUPS_DESTROY, {
            id: id
        })
    }
}
 
tools.deleteAllPresets = function () {
    var presets = $presetList.presets
 
    for (var id in presets) {
        $socket.emit($route.DELETE_PRESET, {
            id: id
        })
    }
}
 
tools.renameVillages = function (name, numeration) {
    var index = 1
    var _name = name
 
    for (var id in $model.getVillages()) {
        if (numeration) {
            _name = $timeHelper.zerofill(index++, 3) + ' ' + name
        }
 
        $socket.emit($route.VILLAGE_CHANGE_NAME, {
            village_id: id,
            name: _name
        })
    }
}
 
var utils = {}
 
utils.getPresetByName = function (name) {
    var presets = $presetList.presets
 
    for (var id in presets) {
        if (presets[id].name === name) {
            return presets[id]
        }
    }
 
    return false
}
 
utils.getGroupByName = function (name) {
    var groups = $model.getGroupList().getGroups()
 
    for (var id in groups) {
        if (groups[id].name === name) {
            return groups[id]
        }
    }
 
    return false
}

// tools.deleteAllGroups()
tools.createGroup('Attack', 257)
tools.createGroup('Defense', 1286)
tools.createGroup('Defense Quick', '0509')
tools.createGroup('Snob', 2058)
tools.createGroup('Church', 2581)
tools.createGroup('Build', '061a')
tools.createGroup('Ignore', '0313')
tools.createGroup('Include', 788)

// tools.deleteAllPresets()
tools.createPreset('Farm (LC)', { light_cavalry: 1 }, '03010c')
tools.createPreset('Farm (MA)', { mounted_archer: 1 }, '04010c')
tools.createPreset('Farm (HC)', { heavy_cavalry: 1 }, '05010c')
tools.createPreset('Farm (SP)', { spear: 5 }, '06010c')
tools.createPreset('Farm (AX)', { axe: 5 }, '09010c')
tools.createPreset('Farm (AR)', { archer: 5 }, '08010c')
tools.createPreset('Farm (SW)', { sword: 5 }, '07010c')
