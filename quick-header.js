$rootScope = injector.get('$rootScope')
transferredSharedDataService = injector.get('transferredSharedDataService')
modelDataService = injector.get('modelDataService')
socketService = injector.get('socketService')
routeProvider = injector.get('routeProvider')
eventTypeProvider = injector.get('eventTypeProvider')
windowDisplayService = injector.get('windowDisplayService')
windowManagerService = injector.get('windowManagerService')
angularHotkeys = injector.get('hotkeys')
armyService = injector.get('armyService')
villageService = injector.get('villageService')
$filter = injector.get('$filter')
$player = modelDataService.getSelectedCharacter()
GAME_STATES = require('conf/gameStates')
$gameState = modelDataService.getGameState()
$mapData = require('struct/MapData')
selectedVillage = $player.getSelectedVillage()
gameData = modelDataService.getGameData()
groupService = injector.get('groupService')
overviewService = injector.get('overviewService')
timeHelper = require('helper/time')
conf = require('conf/conf')
mapconvert = require('helper/mapconvert')
eventQueue = require('queues/EventQueue')
VILLAGE_CONFIG = require('conf/village')
commandService = injector.get('commandService')



socketService.emit(routeProvider.REPORT_GET_LIST_REVERSE, {
    offset: 0,
    count: 100,
    query: '',
    types: [],
    filtersActive: []
}, function (data) {
    data.reports.forEach(function (report) {
        console.log(report.haul)
    })
})


// // LOAD VILLAGE DATA BY ID
// socketService.emit(routeProvider.MAP_GET_VILLAGE_DETAILS, {
//     my_village_id: selectedVillage.getId(),
//     village_id: 9667,
//     num_reports: 1
// }, function (data) {
//     console.log(data)
// })


// // LOAD VILLAGE DATA BY COORDS
// $mapData.getTownAtAsync(500, 500, function (data) {

// })



// $mapData.getTownAtAsync(548, 562, function (data) {
//     console.log(data)
// })



// // CANCEL ALL SELECTED VILLAGE'S COMMANDS.
// selectedVillage.getCommandListModel().getOutgoingCommands(true, true).forEach(function (command) {
//     commandService.isCancelable(command) && socketService.emit(routeProvider.COMMAND_CANCEL, { command_id: command.id })
// })
