/**
* Attack Identifier for Tribal Wars 2
* 
* Rafael Mafra <mafrazzrafael@gmail.com>
*
* 06/2017
*/

require([
    'helper/time',
    'helper/math',
    'conf/commandTypes'
], function ($timeHelper, math, COMMAND_TYPES) {
    var $root = angular.element(document).scope()
    var $route = injector.get('routeProvider')
    var $eventType = injector.get('eventTypeProvider')
    var $armyService = injector.get('armyService')
    var $socket = injector.get('socketService')
    var $windowManagerService = injector.get('windowManagerService')
    var unitOrder = ['axe', 'sword', 'light_cavalry', 'heavy_cavalry', 'ram', 'snob', 'trebuchet']
    var transformUnitName = {
        axe: 'Machado',
        sword: 'Espada',
        light_cavalry: 'Leve',
        heavy_cavalry: 'Pesada',
        ram: 'Ariete',
        snob: 'Nobre',
        trebuchet: 'Trabuco'
    }
    var renameQueue = []
    var method = '0'
    var currentScreen

    function analyse () {
        var isVillageScreen = $windowManagerService.isScreenOpen('screen_village_info')
        var isOverviewScreen = $windowManagerService.isScreenOpen('screen_overview')
        var scope = $windowManagerService.getScopes()[0]
        var $commandRows
        var eachCommandHandler
        var anyChange = false

        if (!isVillageScreen && !isOverviewScreen) {
            return emitNotif('error', 'Execute na visualização "Chegando" ou em uma aldeia na aba "Comandos"')
        }

        if (isVillageScreen) {
            if (scope.activeTab !== 'commands') {
                scope.setActiveTab('commands')

                return setInterval(2000, analyse)
            }

            $commandRows = $('.screen-village-info .tbl-commands:eq(1) tr[class]')

            eachCommandHandler = function ($command) {
                var scope = angular.element($command).scope()
                var command = scope.command

                var slowestUnit = getSlowestUnit(command, 'screen_village_info')
                var transformedName = transformUnitName[slowestUnit]

                updateIcon($command, slowestUnit, 'screen_village_info')
            }
        } else if (isOverviewScreen) {
            if (scope.activeTab !== 'incoming') {
                scope.setActiveTab('incoming')

                return setInterval(2000, analyse)
            }

            $commandRows = $('.screen-overview .incoming table:eq(2) tr')

            eachCommandHandler = function ($command) {
                var scope = angular.element($command).scope()
                var command = scope.command
                var slowestUnit = getSlowestUnit(command, 'screen_overview')
                var transformedName = transformUnitName[slowestUnit]

                if (command.command_name !== transformedName) {
                    renameQueue.push({
                        command: command,
                        name: transformedName
                    })

                    anyChange = true
                }

                updateIcon($command, slowestUnit, 'screen_overview')
            }
        }

        if (!$commandRows.length) {
            return emitNotif('error', 'Nenhuma comando sendo exibido!')
        }

        $commandRows.forEach(eachCommandHandler)

        if (anyChange) {
            method = prompt([
                'IDENTIFICADOR DE COMANDOS\n',
                '0 = temporário, não renomear os comandos',
                '1 = permanente, um comando a cada 2 segundos',
                '2 = permanente, comando a cada 2 ~ 5 segundos',
                '3 = permanente e instantâneo (denúncia = ban)'
            ].join('\n'), 0)

            if (method === '' || isNaN(method) || parseInt(method, 10) > 3) {
                method = '0'
            }

            renameNext()
        } else if (!isVillageScreen) {
            emitNotif('success', 'Todos comandos já foram renomeados.')
        }
    }

    function renameNext () {
        var data = renameQueue.shift()

        if (method !== '0') {
            renameCommand(data.command.command_id, data.name)
        }

        data.command.command_name = data.name

        if (!renameQueue.length) {
            return finishRename()
        }

        switch (method) {
            case '0':
            case '3':
                renameNext()
                break
            case '1':
                setTimeout(renameNext, 2000)
            break
            case '2':
                setTimeout(renameNext, getRandomInt(2, 5) * 1000)
            break
        }
    }

    function finishRename () {
        emitNotif('success', 'Comandos renomeados!')
    }

    function getSlowestUnit (command, screen) {
        var targetVillage
        var originVillage

        if (screen === 'screen_overview') {
            targetVillage = {x: command.target_x, y: command.target_y}
            originVillage = {x: command.origin_x, y: command.origin_y}
        } else if (screen === 'screen_village_info') {
            targetVillage = {x: command.targetX, y: command.targetY}
            originVillage = {x: command.startX, y: command.startY}
        }

        var distanceData = calculateDistanceData(originVillage, targetVillage)
        var travelTime = command.time_completed - command.time_start

        var diffTimes = []

        for (var unit in distanceData.unitArrivals) {
            diffTimes.push({
                unit: unit,
                diff: Math.abs(distanceData.unitArrivals[unit] - travelTime)
            })
        }

        diffTimes = diffTimes.sort(function (a, b) {
            return a.diff - b.diff
        })

        return diffTimes[0].unit
    }

    function calculateDistanceData (originVillage, targetVillage) {
        var unitArrivals = {}
        var army = {
            officers: {}
        }
        var distance = math.actualDistance(targetVillage, {
            x: originVillage.x,
            y: originVillage.y
        })

        for (var i = 0; i < unitOrder.length; i++) {
            army.units = {}
            army.units[unitOrder[i]] = 1

            var travelTimes = $armyService.calculateTravelTime(army, {
                barbarian: false,
                ownTribe: false
            }, COMMAND_TYPES.TYPES.ATTACK)

            unitArrivals[unitOrder[i]] = $armyService.getTravelTimeForDistance(
                army,
                travelTimes,
                distance,
                COMMAND_TYPES.TYPES.ATTACK
            )
        }

        return {
            unitArrivals: unitArrivals,
            distance: distance
        }
    }

    function updateIcon ($command, unit, screen) {
        var selector = screen === 'screen_overview' ? 'span.edit' : 'td:nth-child(2) .size-34x34'
        var typeClassList = $command.querySelector(selector).classList

        if (screen === 'screen_overview') {
            typeClassList.remove('btn-orange')
        } else if (screen === 'screen_village_info') {
            typeClassList.remove('icon-26x26-time-duration')
        }

        typeClassList.add('icon-34x34-unit-' + unit)
    }

    function renameCommand (commandId, name) {
        $socket.emit($route.COMMAND_RENAME, {
            command_id: commandId,
            name: name
        })
    }

    function getRandomInt (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function emitNotif (type, message) {
        var eventType = type === 'success'
            ? $eventType.MESSAGE_SUCCESS
            : $eventType.MESSAGE_ERROR

        $root.$broadcast(eventType, {
            message: message
        })
    }

    analyse()
})
